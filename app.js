const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  console.log(req)
  res.json({
    id: 2000,
    name: 'iPhone'
  })
})

app.get('/hellome', (req, res) => {
  res.send('Hello Me')
})

app.listen(port, () => {
  console.log( `Example app listen on port ${port}`)
})